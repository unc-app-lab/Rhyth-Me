﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpeedController : MonoBehaviour
{
    [SerializeField] private MovementController player ;
    // Start is called before the first frame update
     private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name.Equals("MC") && !other.isTrigger){
           player.speed = 10f;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.name.Equals("MC") && !other.isTrigger) {
            player.speed = 4f;
        }
    }

    // Update is called once per frame
    
}
