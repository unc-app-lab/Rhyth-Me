﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level_Exit : MonoBehaviour
{
    [SerializeField] private int scenecount = 0; 
    void OnTriggerEnter2D(Collider2D col) {
        if(col.gameObject.tag == "Player"){
        SceneManager.LoadScene(scenecount + 1);
        }
    }
        
}
