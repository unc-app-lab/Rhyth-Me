﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class switchController : MonoBehaviour
{

    private SpriteRenderer localRenderer;
    [SerializeField] private Sprite off_switch;
    [SerializeField] private Sprite on_switch;
    private bool status = false;
    public GameObject death;
    // Start is called before the first frame update
    void Start()
    {
        localRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D (Collider2D other) {
        if (other.name.Equals("MC") && !other.isTrigger){
            status = !status;
            controlLever();
            leverAction();
        }
    }

    private void controlLever (){
        if (status) {
            localRenderer.sprite = on_switch;
        }
    }

    // Connecting Levers to Cages/Actions is as follows
        //Assign the lever a number tag
        //Assign the object that is going to be manipulating a "lever_#" tag
        //Assign the parent object a specific tag regarding functionality 
        //Grab an object that matches the number tag, check the parent for functioanlity , execute functionality

        //SPECIAL NOTE:
        //In instances where the action will cause the player to be trapped, a Death_Spawn object in the 
        //cage prefab will need to be put in the appropiate location in the Unity Scene.
        //This object is empty and only used to get the appropiate location for the Death prefab.

        //Player_Blocker is necessary to stop the player from accidentally activating the gravity cage too early

    private void leverAction () {

        var tag = gameObject.tag ;
        var localObject = GameObject.FindWithTag("lever_" + tag);
        var parentObject = localObject.transform.parent;
        if (parentObject.tag == "coin" ){
            Destroy(localObject);
            Destroy(parentObject.transform.Find("Player_Blocker_"+tag).gameObject);
        }

        if (parentObject.tag == "gravity") {
            Destroy(parentObject.transform.Find("Player_Blocker_"+tag).gameObject);
            localObject.GetComponent<Rigidbody2D>().WakeUp();
            var deathSpawn = GameObject.FindWithTag("Death_Spawn_" + tag).transform.position;
            Instantiate(death, new Vector2(deathSpawn.x,deathSpawn.y), Quaternion.identity);
        }
        
     
    }
}
