﻿
public interface IDamage<T>
{
    T health { get; set; }
    void ApplyDamage(T damageAmount); 
}