﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour
{
    bool hitObject = false;
    [SerializeField] private Collider2D attackBox;
    [SerializeField] private Animator animator;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && !animator.GetBool("Attacking"))
        {
            animator.SetBool("Attacking", true);
            CheckForHit();
        }
    }

    private void CheckForHit()
    {
        int resultCount = 5;
        Collider2D[] hitColliders = new Collider2D[5];
        ContactFilter2D contactFilter = new ContactFilter2D();
        resultCount = attackBox.OverlapCollider(contactFilter, hitColliders);
        Debug.Log("Hits: " + resultCount);
        foreach (Collider2D collider in hitColliders)
        {
            if (collider == null) { continue; }
            Debug.Log("Overlap: " + collider.name);
            IDamage<float> damageInterface = (IDamage<float>)collider.GetComponent<IDamage<float>>();
            if (damageInterface != null)
            {
                Debug.Log("Hit!");
                damageInterface.ApplyDamage(60.0f);
            }

        }
    }
    
}
